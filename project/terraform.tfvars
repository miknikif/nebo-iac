project_name           = "nebo"
master_billing_account = "013994-732700-2FC0E8"
lien                   = true
labels = {
  "owner" = "mykhailo-nikiforov",
  "env"   = "production"
}

#Provide list of apis for this project 
activate_apis = [
  "iam.googleapis.com",
  "run.googleapis.com",
  "container.googleapis.com",
  "secretmanager.googleapis.com",
  "anthos.googleapis.com",
  "meshconfig.googleapis.com",
  "meshca.googleapis.com",
  "gkehub.googleapis.com",
  "cloudresourcemanager.googleapis.com",
  "cloudfunctions.googleapis.com",
  "cloudbuild.googleapis.com",
  "cloudscheduler.googleapis.com"
]
