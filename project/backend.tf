terraform {
  backend "gcs" {
    bucket = "tf-state-nebo-us-east-1"
    prefix = "project"
  }
}
