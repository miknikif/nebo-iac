# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version     = "3.90.1"
  constraints = ">= 3.43.0, >= 3.50.0, >= 3.90.1, < 4.0.0"
  hashes = [
    "h1:9TYwyR4R4dIop7wV2lvvYZHw9RUVd/YRWR+9jjXpyfw=",
  ]
}

provider "registry.terraform.io/hashicorp/google-beta" {
  version     = "3.90.1"
  constraints = ">= 3.1.0, >= 3.43.0, >= 3.50.0, < 4.0.0"
  hashes = [
    "h1:GYvCJnoIaT9clM2aAbfe0qsnRy4OdRfJ0NwO0dWnCJI=",
  ]
}

provider "registry.terraform.io/hashicorp/null" {
  version     = "2.1.2"
  constraints = "~> 2.1"
  hashes = [
    "h1:l0/ASa/TB1exgqdi33sOkFakJL2zAQBu+q5LgO5/SxI=",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version     = "2.3.1"
  constraints = "~> 2.2"
  hashes = [
    "h1:3L/rQ5ZcgRwOEDHogAIuYYZsh8gB9hpciLBnhyna6f0=",
  ]
}
