/**
  * ## Project module
  * ### Description
  * This execution point automates the provision of GCP project.
  * A Google Cloud Platform (GCP) project is a set of configuration settings that define how your app interacts with
  * Google services and what resources it uses.
  * ### File structure
  * - `backend.tf` - This configuration specifies a backend, which defines where state snapshots are stored. In our case
  * it is in the GCS bucket. Attribute "prefix" gives the path where the state files are stored.
  * - `main.tf` - Contains this description information.
  * - `outputs.tf` - Has output variables which are displayed upon
  * successful creation of the resources and can be used in other modules.
  * - `project.tf` - Describes custom module to create GCP project.
  * - `provider.tf` - In this file the provider information is declared.
  * Terraform relies on plugins called "providers" to interact with cloud providers, SaaS providers, and other APIs.
  * - `terraform.tfvars` - Holds the values of variables which are needed for successful creation of the resources.
  * - `variables.tf` - Defines variables and their default values that are needed for successful creation of the resources.
  * - `version.tf` - Ensuring that we will be able to use only v1.2.5
  */

module "project" {
  source                  = "terraform-google-modules/project-factory/google"
  version                 = "~> 10.1"
  name                    = var.project_name
  org_id                  = null
  random_project_id       = true
  billing_account         = var.master_billing_account
  default_service_account = "keep"
  lien                    = var.lien
  activate_apis           = var.activate_apis
  labels                  = var.labels
  create_project_sa       = false
}
