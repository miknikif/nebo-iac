<!-- BEGIN_TF_DOCS -->
## Project module
### Description
This execution point automates the provision of GCP project.
A Google Cloud Platform (GCP) project is a set of configuration settings that define how your app interacts with
Google services and what resources it uses.
### File structure
- `backend.tf` - This configuration specifies a backend, which defines where state snapshots are stored. In our case
it is in the GCS bucket. Attribute "prefix" gives the path where the state files are stored.
- `main.tf` - Contains this description information.
- `outputs.tf` - Has output variables which are displayed upon
successful creation of the resources and can be used in other modules.
- `project.tf` - Describes custom module to create GCP project.
- `provider.tf` - In this file the provider information is declared.
Terraform relies on plugins called "providers" to interact with cloud providers, SaaS providers, and other APIs.
- `terraform.tfvars` - Holds the values of variables which are needed for successful creation of the resources.
- `variables.tf` - Defines variables and their default values that are needed for successful creation of the resources.
- `version.tf` - Ensuring that we will be able to use only v1.2.5

### Requirements

| Name | Version |
|------|---------|
| terraform | = 1.2.5 |
| google | >= 3.90.1 |

### Providers

No providers.

### Resources

No resources.

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| activate\_apis | n/a | `list(string)` | `[]` | no |
| labels | n/a | `map(string)` | `{}` | no |
| lien | n/a | `bool` | `true` | no |
| master\_billing\_account | n/a | `string` | `null` | no |
| project\_name | n/a | `string` | `""` | no |

### Modules

| Name | Source | Version |
|------|--------|---------|
| project | terraform-google-modules/project-factory/google | ~> 10.1 |

### Outputs

| Name | Description |
|------|-------------|
| project\_id | The ID of the project |
| project\_name | The name of the project |
| service\_account\_email | The email of the default service account |



---

> Written by the [SoftServe](https://softserveinc.com) team
<!-- END_TF_DOCS -->