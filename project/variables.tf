variable "master_billing_account" {
  type    = string
  default = null
}

variable "project_name" {
  type    = string
  default = ""
}

variable "lien" {
  type    = bool
  default = true
}

variable "activate_apis" {
  type    = list(string)
  default = []
}

variable "labels" {
  type    = map(string)
  default = {}
}
