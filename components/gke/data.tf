resource "random_pet" "gke" {
  prefix = "gke-nebo"
  length = 2
}

resource "random_pet" "vpc" {
  prefix = "gke-vpc"
  length = 2
}

resource "random_pet" "node_pool" {
  prefix = "node-pool"
  length = 2
}

resource "random_pet" "ns" {
  prefix = "ns"
  length = 2
}

resource "random_pet" "ns_istio" {
  prefix = "ns-istio"
  length = 2
}

data "google_client_config" "default" {}


data "google_project" "project" {
  project_id = data.terraform_remote_state.project.outputs.project_id
}
