resource "kubernetes_deployment" "asm_ingress" {
  metadata {
    name      = kubernetes_manifest.sc_asm_ingress.manifest.metadata.name
    namespace = kubernetes_namespace.ns_istio_ingress.metadata.0.name
  }

  spec {
    replicas = 3
    selector {
      match_labels = local.asm_labels
    }

    template {
      metadata {
        annotations = {
          "inject.istio.io/templates" = "gateway"
        }
        labels = local.asm_labels
      }

      spec {
        automount_service_account_token = false
        enable_service_links            = false
        container {
          image = "auto"
          name  = "istio-proxy"

          resources {
            limits = {
              cpu    = "2000m"
              memory = "1024Mi"
            }
            requests = {
              cpu    = "100m"
              memory = "128Mi"
            }
          }
        }
        service_account_name = kubernetes_manifest.sc_asm_ingress.manifest.metadata.name
      }
    }
  }
}
