resource "google_compute_global_address" "asm_ingress" {
  name    = "ingress-ip"
  project = data.terraform_remote_state.project.outputs.project_id
}
