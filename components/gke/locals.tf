locals {
  istio_inject_labels = {
    "istio-injection" = "enabled"
    "istio.io/rev"    = "asm-managed"
  }

  asm_labels = {
    asm = "ingressgateway"
  }

  pod_security_labels = {
    "security-istio/status"        = "enabled"
    "security-istio/port-disallow" = "8080"
  }
}
