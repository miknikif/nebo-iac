resource "kubernetes_ingress_v1" "asm_ingress" {
  metadata {
    name      = "gke-ingress"
    namespace = kubernetes_namespace.ns_istio_ingress.metadata.0.name
    annotations = {
      "kubernetes.io/ingress.allow-http"            = "false"
      "kubernetes.io/ingress.global-static-ip-name" = google_compute_global_address.asm_ingress.name
      "networking.gke.io/managed-certificates"      = kubernetes_manifest.mc_asm_ingress.manifest.metadata.name
      "kubernetes.io/ingress.class"                 = "gce"
    }
  }

  spec {
    default_backend {
      service {
        name = kubernetes_service.asm_ingress.metadata.0.name
        port {
          number = 443
        }
      }
    }
    rule {
      http {
        path {
          backend {
            service {
              name = kubernetes_service.asm_ingress.metadata.0.name
              port {
                number = 443
              }
            }
          }
          path_type = "ImplementationSpecific"
          path      = "/*"
        }
      }
    }
  }
}
