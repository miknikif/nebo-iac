module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google"
  project_id                 = data.terraform_remote_state.project.outputs.project_id
  name                       = random_pet.gke.id
  region                     = var.region
  regional                   = false
  zones                      = var.cluster_zones
  network                    = data.terraform_remote_state.vpc.outputs.gke_vpc.network_name
  subnetwork                 = data.terraform_remote_state.vpc.outputs.gke_vpc.subnets_names[0]
  ip_range_pods              = "${data.terraform_remote_state.vpc.outputs.gke_vpc.subnets_names[0]}-pods"
  ip_range_services          = "${data.terraform_remote_state.vpc.outputs.gke_vpc.subnets_names[0]}-services"
  cluster_resource_labels    = { "mesh_id" : "proj-${data.google_project.project.number}" }
  identity_namespace         = "${data.terraform_remote_state.project.outputs.project_id}.svc.id.goog"
  http_load_balancing        = true
  network_policy             = true
  horizontal_pod_autoscaling = true
  filestore_csi_driver       = false
  remove_default_node_pool   = true

  node_pools = [
    {
      name               = random_pet.node_pool.id
      machine_type       = var.node_pool_instance_type
      node_locations     = join(",", var.cluster_zones)
      min_count          = var.node_pool_min_instances
      max_count          = var.node_pool_max_instances
      local_ssd_count    = 0
      spot               = false
      disk_size_gb       = 50
      disk_type          = "pd-standard"
      image_type         = "COS_CONTAINERD"
      enable_gcfs        = false
      enable_gvnic       = false
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = false
      initial_node_count = var.node_pool_min_instances
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    "${random_pet.node_pool.id}" = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  node_pools_labels = {
    all = {}

    "${random_pet.node_pool.id}" = {
      default-node-pool = true
    }
  }
}

module "asm" {
  source = "terraform-google-modules/kubernetes-engine/google//modules/asm"

  project_id                = data.terraform_remote_state.project.outputs.project_id
  cluster_name              = module.gke.name
  cluster_location          = module.gke.location
  multicluster_mode         = "connected"
  enable_cni                = true
  enable_fleet_registration = true
  enable_mesh_feature       = true
}
