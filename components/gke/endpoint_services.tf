resource "google_endpoints_service" "asm_ingress" {
  service_name   = "frontend.endpoints.${data.terraform_remote_state.project.outputs.project_id}.cloud.goog"
  project        = data.terraform_remote_state.project.outputs.project_id
  openapi_config = <<-EOF
  swagger: "2.0"
  info:
    description: "Cloud Endpoints DNS"
    title: "Cloud Endpoints DNS"
    version: "1.0.0"
  paths: {}
  host: "frontend.endpoints.${data.terraform_remote_state.project.outputs.project_id}.cloud.goog"
  x-google-endpoints:
  - name: "frontend.endpoints.${data.terraform_remote_state.project.outputs.project_id}.cloud.goog"
    target: "${google_compute_global_address.asm_ingress.address}"
  EOF
}
