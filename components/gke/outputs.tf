output "basic_auth_login" {
  description = "Basic auth login"
  value       = var.basic_auth_login
}

output "basic_auth_pass" {
  description = "Basic auth pass"
  value       = var.basic_auth_password
  sensitive   = true
}

output "app_url" {
  description = "APP URL"
  value       = "https://frontend.endpoints.${data.terraform_remote_state.project.outputs.project_id}.cloud.goog"
}
