<!-- BEGIN_TF_DOCS -->


### Requirements

| Name | Version |
|------|---------|
| terraform | = 1.2.5 |
| google | >= 4.29.0 |
| helm | 2.6.0 |
| kubernetes | 2.12.1 |
| random | 3.3.2 |
| tls | 4.0.1 |

### Providers

| Name | Version |
|------|---------|
| google | >= 4.29.0 |
| helm | 2.6.0 |
| kubernetes | 2.12.1 |
| random | 3.3.2 |
| terraform | n/a |
| tls | 4.0.1 |

### Resources

| Name | Type |
|------|------|
| [google_compute_global_address.asm_ingress](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_global_address) | resource |
| [google_compute_security_policy.asm_ingress](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_security_policy) | resource |
| [google_endpoints_service.asm_ingress](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/endpoints_service) | resource |
| [google_monitoring_alert_policy.app_health](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/monitoring_alert_policy) | resource |
| [google_monitoring_notification_channel.email](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/monitoring_notification_channel) | resource |
| [helm_release.httpd_istio](https://registry.terraform.io/providers/hashicorp/helm/2.6.0/docs/resources/release) | resource |
| [kubernetes_deployment.asm_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/deployment) | resource |
| [kubernetes_horizontal_pod_autoscaler.asm_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/horizontal_pod_autoscaler) | resource |
| [kubernetes_ingress_v1.asm_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/ingress_v1) | resource |
| [kubernetes_manifest.ap_sallow_port_80](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/manifest) | resource |
| [kubernetes_manifest.backend_config_asm_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/manifest) | resource |
| [kubernetes_manifest.gw_asm](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/manifest) | resource |
| [kubernetes_manifest.mc_asm_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/manifest) | resource |
| [kubernetes_manifest.sc_asm_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/manifest) | resource |
| [kubernetes_manifest.vs_httpd](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/manifest) | resource |
| [kubernetes_namespace.ns](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/namespace) | resource |
| [kubernetes_namespace.ns_istio](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/namespace) | resource |
| [kubernetes_namespace.ns_istio_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/namespace) | resource |
| [kubernetes_role.asm_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/role) | resource |
| [kubernetes_role_binding.asm_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/role_binding) | resource |
| [kubernetes_secret.asm_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/secret) | resource |
| [kubernetes_secret.edge2mesh_credential](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/secret) | resource |
| [kubernetes_service.asm_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/2.12.1/docs/resources/service) | resource |
| [random_pet.gke](https://registry.terraform.io/providers/hashicorp/random/3.3.2/docs/resources/pet) | resource |
| [random_pet.node_pool](https://registry.terraform.io/providers/hashicorp/random/3.3.2/docs/resources/pet) | resource |
| [random_pet.ns](https://registry.terraform.io/providers/hashicorp/random/3.3.2/docs/resources/pet) | resource |
| [random_pet.ns_istio](https://registry.terraform.io/providers/hashicorp/random/3.3.2/docs/resources/pet) | resource |
| [random_pet.vpc](https://registry.terraform.io/providers/hashicorp/random/3.3.2/docs/resources/pet) | resource |
| [tls_private_key.private_key](https://registry.terraform.io/providers/hashicorp/tls/4.0.1/docs/resources/private_key) | resource |
| [tls_self_signed_cert.certificate](https://registry.terraform.io/providers/hashicorp/tls/4.0.1/docs/resources/self_signed_cert) | resource |
| [google_client_config.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/client_config) | data source |
| [google_project.project](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/project) | data source |
| [terraform_remote_state.project](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |
| [terraform_remote_state.vpc](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| basic\_auth\_enabled | n/a | `string` | `""` | no |
| basic\_auth\_login | n/a | `string` | `""` | no |
| basic\_auth\_password | n/a | `string` | `""` | no |
| cluster\_zones | n/a | `list(string)` | `[]` | no |
| helm\_image\_repository | n/a | `string` | `""` | no |
| helm\_image\_tag | n/a | `string` | `""` | no |
| helm\_url | n/a | `string` | `""` | no |
| node\_pool\_instance\_type | n/a | `string` | `"e2-medium"` | no |
| node\_pool\_max\_instances | n/a | `number` | `3` | no |
| node\_pool\_min\_instances | n/a | `number` | `1` | no |
| region | n/a | `string` | `"us-east1"` | no |

### Modules

| Name | Source | Version |
|------|--------|---------|
| asm | terraform-google-modules/kubernetes-engine/google//modules/asm | n/a |
| gke | terraform-google-modules/kubernetes-engine/google | n/a |

### Outputs

| Name | Description |
|------|-------------|
| app\_url | APP URL |
| basic\_auth\_login | Basic auth login |
| basic\_auth\_pass | Basic auth pass |



---

> Written by the [SoftServe](https://softserveinc.com) team
<!-- END_TF_DOCS -->