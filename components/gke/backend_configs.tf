resource "kubernetes_manifest" "backend_config_asm_ingress" {
  manifest = {
    "apiVersion" = "cloud.google.com/v1"
    "kind"       = "BackendConfig"
    "metadata" = {
      "namespace" = kubernetes_namespace.ns_istio_ingress.metadata.0.name
      "name"      = "ingress-backendconfig"
    }
    "spec" = {
      "healthCheck" = {
        "requestPath" = "/healthz/ready"
        "port"        = "15021"
        "type"        = "HTTP"
      }
      "securityPolicy" = {
        "name" = "edge-fw-policy"
      }
    }
  }
}
