resource "kubernetes_role" "asm_ingress" {
  metadata {
    name      = kubernetes_manifest.sc_asm_ingress.manifest.metadata.name
    namespace = kubernetes_namespace.ns_istio_ingress.metadata.0.name
  }

  rule {
    api_groups = [""]
    resources  = ["secrets"]
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_role_binding" "asm_ingress" {
  metadata {
    name      = kubernetes_manifest.sc_asm_ingress.manifest.metadata.name
    namespace = kubernetes_namespace.ns_istio_ingress.metadata.0.name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.asm_ingress.metadata.0.name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_manifest.sc_asm_ingress.manifest.metadata.name
    namespace = kubernetes_namespace.ns_istio_ingress.metadata.0.name
  }
}
