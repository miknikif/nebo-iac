resource "kubernetes_manifest" "ap_sallow_port_80" {
  manifest = {
    "apiVersion" = "security.istio.io/v1beta1"
    "kind"       = "AuthorizationPolicy"
    "metadata" = {
      "namespace" = kubernetes_namespace.ns_istio.metadata.0.name
      "name"      = "allow-port-80"
    }
    "spec" = {
      "selector" = {
        "matchLabels" = local.pod_security_labels
      }
      "action" = "DENY"
      "rules" = [{
        "to" = [{
          "operation" = {
            "ports" = [local.pod_security_labels["security-istio/port-disallow"]]
          }
        }]
      }]
    }
  }
}
