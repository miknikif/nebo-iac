resource "google_compute_security_policy" "asm_ingress" {
  name        = "edge-fw-policy"
  description = "Block XSS attacks"
  project     = data.terraform_remote_state.project.outputs.project_id

  rule {
    action   = "deny(403)"
    priority = "1000"
    match {
      expr {
        expression = "evaluatePreconfiguredExpr('xss-stable')"
      }
    }
    description = "XSS attack filtering"
  }

  rule {
    action      = "allow"
    description = "default rule"
    preview     = false
    priority    = 2147483647
    match {
      versioned_expr = "SRC_IPS_V1"
      config {
        src_ip_ranges = [
          "*",
        ]
      }
    }
  }
}
