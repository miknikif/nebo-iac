resource "google_monitoring_notification_channel" "email" {
  display_name = "devops team"
  type         = "email"
  labels = {
    email_address = "u@xaked.com"
  }
}
