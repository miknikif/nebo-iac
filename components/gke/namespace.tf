resource "kubernetes_namespace" "ns" {
  metadata {
    name = random_pet.ns.id
  }
}

resource "kubernetes_namespace" "ns_istio" {
  metadata {
    name   = random_pet.ns_istio.id
    labels = local.istio_inject_labels
  }
}

resource "kubernetes_namespace" "ns_istio_ingress" {
  metadata {
    name   = "asm-ingress"
    labels = local.istio_inject_labels
  }
}
