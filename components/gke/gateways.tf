resource "kubernetes_manifest" "gw_asm" {
  manifest = {
    "apiVersion" = "networking.istio.io/v1alpha3"
    "kind"       = "Gateway"
    "metadata" = {
      "namespace" = kubernetes_namespace.ns_istio_ingress.metadata.0.name
      "name"      = "asm-ingressgateway"
    }
    "spec" = {
      "selector" = local.asm_labels
      "servers" = [{
        "port" = {
          "number"   = 443
          "name"     = "https"
          "protocol" = "HTTPS"
        }
        "hosts" = ["*"]
        "tls" = {
          "mode"           = "SIMPLE"
          "credentialName" = kubernetes_secret.edge2mesh_credential.metadata.0.name
        }
      }]
    }
  }
}
