region                  = "us-east1"
cluster_zones           = ["us-east1-b"]
node_pool_instance_type = "e2-standard-4"
node_pool_min_instances = 1
node_pool_max_instances = 3

// helm
helm_url              = "https://gitlab.com/miknikif/nebo-tasks-helm/-/raw/6371d63e4d9791b88c08981e141b78418540d5b6/helm/apache.tgz?inline=false"
helm_image_repository = "xaked/nebo_httpd"
helm_image_tag        = "2.4.54-alpine"
basic_auth_enabled    = true
