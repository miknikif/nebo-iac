resource "kubernetes_horizontal_pod_autoscaler" "asm_ingress" {
  metadata {
    name      = kubernetes_manifest.sc_asm_ingress.manifest.metadata.name
    namespace = kubernetes_namespace.ns_istio_ingress.metadata.0.name
  }

  spec {
    max_replicas = 5
    min_replicas = 3

    metric {
      type = "Resource"
      resource {
        name = "cpu"
        target {
          type                = "Utilization"
          average_utilization = 80
        }
      }
    }

    behavior {
      scale_down {
        select_policy                = "Max"
        stabilization_window_seconds = 0
        policy {
          period_seconds = 15
          type           = "Percent"
          value          = 100
        }
      }
      scale_up {
        select_policy                = "Max"
        stabilization_window_seconds = 0
        policy {
          period_seconds = 15
          type           = "Pods"
          value          = 4
        }
        policy {
          period_seconds = 15
          type           = "Percent"
          value          = 100
        }
      }
    }

    scale_target_ref {
      api_version = "apps/v1"
      kind        = "Deployment"
      name        = kubernetes_deployment.asm_ingress.metadata.0.name
    }
  }
}
