resource "kubernetes_manifest" "mc_asm_ingress" {
  manifest = {
    "apiVersion" = "networking.gke.io/v1"
    "kind"       = "ManagedCertificate"
    "metadata" = {
      "namespace" = kubernetes_namespace.ns_istio_ingress.metadata.0.name
      "name"      = "gke-ingress-cert"
    }
    "spec" = {
      "domains" = ["frontend.endpoints.${data.terraform_remote_state.project.outputs.project_id}.cloud.goog"]
    }
  }
}
