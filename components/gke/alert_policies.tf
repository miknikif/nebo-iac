resource "google_monitoring_alert_policy" "app_health" {
  display_name          = "Kubernetes Container - Restart count for httpd, apache by label.pod_name [SUM]"
  combiner              = "OR"
  notification_channels = [google_monitoring_notification_channel.email.name]
  alert_strategy {
    auto_close = "86400s"
  }
  documentation {
    content   = "Check our app. Looks like it's restarting..."
    mime_type = "text/markdown"
  }
  conditions {
    display_name = "Kubernetes Container - Restart count for httpd, apache by label.pod_name [SUM]"
    condition_threshold {
      duration   = "0s"
      filter     = "resource.type = \"k8s_container\" AND (resource.labels.container_name = \"apache\" AND resource.labels.namespace_name = \"${kubernetes_namespace.ns_istio.metadata.0.name}\") AND metric.type = \"kubernetes.io/container/restart_count\""
      comparison = "COMPARISON_GT"
      aggregations {
        alignment_period     = "3600s"
        cross_series_reducer = "REDUCE_COUNT"
        group_by_fields = [
          "resource.label.pod_name",
        ]
        per_series_aligner = "ALIGN_DELTA"
      }
      trigger {
        count   = 1
        percent = 0
      }
    }
  }
}
