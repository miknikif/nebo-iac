# resource "helm_release" "httpd" {
#   name      = "httpd"
#   chart     = var.helm_url
#   namespace = kubernetes_namespace.ns.metadata.0.name

#   values = [<<-EOF
#     replicaCount: 3
#     image:
#       repository: "${var.helm_image_repository}"
#       pullPolicy: Always
#       tag: "${var.helm_image_tag}"
#     nameOverride: "httpd"
#     service:
#       type: LoadBalancer
#       port: 80
#     resources:
#       limits:
#           cpu: 100m
#           memory: 128Mi
#       requests:
#           cpu: 100m
#           memory: 128Mi
#     basicAuth:
#       enabled: ${var.basic_auth_enabled}
#       username: ${var.basic_auth_login}
#       password: ${var.basic_auth_password}
#     EOF
#   ]
# }

resource "helm_release" "httpd_istio" {
  name      = "httpd"
  chart     = var.helm_url
  namespace = kubernetes_namespace.ns_istio.metadata.0.name

  values = [<<-EOF
    replicaCount: 3
    image:
      repository: "${var.helm_image_repository}"
      pullPolicy: Always
      tag: "${var.helm_image_tag}"
    nameOverride: "httpd"
    service:
      type: ClusterIP
      port: 80
    resources:
      limits:
          cpu: 100m
          memory: 128Mi
      requests:
          cpu: 100m
          memory: 128Mi
    basicAuth:
      enabled: ${var.basic_auth_enabled}
      username: ${var.basic_auth_login}
      password: ${var.basic_auth_password}
    ${yamlencode({ "podLabels" : local.pod_security_labels })}
    EOF
  ]
}
