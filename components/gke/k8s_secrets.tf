resource "kubernetes_manifest" "sc_asm_ingress" {
  manifest = {
    "apiVersion" = "v1"
    "kind"       = "ServiceAccount"
    "metadata" = {
      "namespace" = kubernetes_namespace.ns_istio_ingress.metadata.0.name
      "name"      = "asm-ingressgateway"
    }
  }
}

resource "kubernetes_secret" "asm_ingress" {
  metadata {
    name      = "${kubernetes_manifest.sc_asm_ingress.manifest.metadata.name}-token-secret"
    namespace = kubernetes_namespace.ns_istio_ingress.metadata.0.name
    annotations = {
      "kubernetes.io/service-account.name"      = kubernetes_manifest.sc_asm_ingress.manifest.metadata.name
      "kubernetes.io/service-account.namespace" = kubernetes_namespace.ns_istio_ingress.metadata.0.name
    }
  }
  type = "kubernetes.io/service-account-token"
}

resource "kubernetes_secret" "edge2mesh_credential" {
  metadata {
    name      = "edge2mesh-credential"
    namespace = kubernetes_namespace.ns_istio_ingress.metadata.0.name
  }

  data = {
    "tls.key" = trimspace(tls_private_key.private_key.private_key_pem)
    "tls.crt" = trimspace(tls_self_signed_cert.certificate.cert_pem)
  }

  type = "kubernetes.io/tls"
}
