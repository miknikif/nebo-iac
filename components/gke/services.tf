resource "kubernetes_service" "asm_ingress" {
  metadata {
    name      = kubernetes_manifest.sc_asm_ingress.manifest.metadata.name
    namespace = kubernetes_namespace.ns_istio_ingress.metadata.0.name
    annotations = {
      "cloud.google.com/neg"            = jsonencode({ "ingress" = true })
      "cloud.google.com/backend-config" = jsonencode({ "default" = "ingress-backendconfig" })
      "cloud.google.com/app-protocols"  = jsonencode({ "https" = "HTTP2" })
    }
    labels = local.asm_labels
  }

  spec {
    selector = local.asm_labels
    port {
      name        = "status-port"
      port        = 15021
      target_port = 15021
      protocol    = "TCP"
    }
    port {
      name        = "http2"
      port        = 80
      target_port = 80
      protocol    = "TCP"
    }
    port {
      name        = "https"
      port        = 443
      target_port = 443
      protocol    = "TCP"
    }

    type = "ClusterIP"
  }

  lifecycle {
    ignore_changes = [metadata.0.annotations["cloud.google.com/neg-status"]]
  }
}
