resource "kubernetes_manifest" "vs_httpd" {
  manifest = {
    "apiVersion" = "networking.istio.io/v1alpha3"
    "kind"       = "VirtualService"
    "metadata" = {
      "namespace" = kubernetes_namespace.ns_istio.metadata.0.name
      "name"      = "frontend-ingress"
    }
    "spec" = {
      "hosts"    = ["frontend.endpoints.${data.terraform_remote_state.project.outputs.project_id}.cloud.goog"]
      "gateways" = ["${kubernetes_namespace.ns_istio_ingress.metadata.0.name}/${kubernetes_manifest.gw_asm.manifest.metadata.name}"]
      "http" = [{
        "route" = [{
          "destination" = {
            "host" = helm_release.httpd_istio.name
            "port" = {
              "number" = 80
            }
          }
        }]
      }]
    }
  }
}
