variable "node_pool_instance_type" {
  type    = string
  default = "e2-medium"
}

variable "node_pool_min_instances" {
  type    = number
  default = 1
}

variable "node_pool_max_instances" {
  type    = number
  default = 3
}

variable "region" {
  type    = string
  default = "us-east1"
}

variable "cluster_zones" {
  type    = list(string)
  default = []
}

variable "helm_url" {
  type    = string
  default = ""
}

variable "helm_image_repository" {
  type    = string
  default = ""
}

variable "helm_image_tag" {
  type    = string
  default = ""
}

variable "basic_auth_enabled" {
  type    = string
  default = ""
}

variable "basic_auth_login" {
  type    = string
  default = ""
}

variable "basic_auth_password" {
  type    = string
  default = ""
}

# variable "autoscaling" {
#   type = object({
#     min = number
#     max = number
#   })
#   default = {
#     min = 0
#     max = 100
#   }
# }

# variable "image" {
#   type    = string
#   default = "gcr.io/nebo-03c2/webapp:v0.0.1"
# }

# variable "env" {
#   type    = list(object({
#     name = string
#     value = string
#   }))
#   default = []
# }

# variable "cpu_alloc" {
#   type    = string
#   default = "1000m"
# }

# variable "mem_alloc" {
#   type    = string
#   default = "128Mi"
# }

# variable "container_concurrency" {
#   type    = number
#   default = 80
# }

# variable "timeout_seconds" {
#   type    = number
#   default = 10
# }

# variable "ports" {
#   type    = list(number)
#   default = [80]
# }

