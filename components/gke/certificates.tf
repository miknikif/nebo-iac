resource "tls_private_key" "private_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "tls_self_signed_cert" "certificate" {
  private_key_pem = tls_private_key.private_key.private_key_pem

  subject {
    common_name  = "frontend.endpoints.${data.terraform_remote_state.project.outputs.project_id}.cloud.goog"
    organization = "Edge2Mesh Inc"
  }

  validity_period_hours = 8760

  allowed_uses = ["any_extended", "cert_signing", "client_auth", "code_signing", "content_commitment", "crl_signing", "data_encipherment", "decipher_only", "digital_signature", "email_protection", "encipher_only", "ipsec_end_system", "ipsec_tunnel", "ipsec_user", "key_agreement", "key_encipherment", "microsoft_commercial_code_signing", "microsoft_kernel_code_signing", "microsoft_server_gated_crypto", "netscape_server_gated_crypto", "ocsp_signing", "server_auth", "timestamping"]
}
