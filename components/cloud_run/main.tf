resource "google_cloud_run_service" "webapp" {
  name     = var.service_name
  location = var.service_region
  project  = data.terraform_remote_state.project.outputs.project_id
  template {
    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = var.autoscaling.min
        "autoscaling.knative.dev/maxScale" = var.autoscaling.max
        "run.googleapis.com/client-name"   = "cloud-console"
      }
      name = "${var.service_name}-${formatdate("YYYYMMDDhhmmss", timestamp())}"
    }
    spec {
      containers {
        image = var.image
        dynamic "env" {
          for_each = var.env

          content {
            name = env.key

            value_from {
              secret_key_ref {
                key  = element(split("/", google_secret_manager_secret_version.secrets[env.key].id), length(split("/", google_secret_manager_secret_version.secrets[env.key].id)) - 1)
                name = env.key
              }
            }
          }
        }
        resources {
          limits = {
            cpu    = var.cpu_alloc
            memory = var.mem_alloc
          }
        }
        dynamic "ports" {
          for_each = var.ports
          content {
            container_port = ports.value
          }
        }
      }
      container_concurrency = var.container_concurrency
      timeout_seconds       = var.timeout_seconds
      service_account_name  = google_service_account.cloud_run_sc.email
    }
  }
  traffic {
    percent         = 100
    latest_revision = true
  }
}
