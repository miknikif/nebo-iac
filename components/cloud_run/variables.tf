variable "service_name" {
  type    = string
  default = "app"
}

variable "service_region" {
  type    = string
  default = "us-east1"
}

variable "autoscaling" {
  type = object({
    min = number
    max = number
  })
  default = {
    min = 0
    max = 100
  }
}

variable "image" {
  type    = string
  default = "gcr.io/nebo-03c2/webapp:v0.0.1"
}

variable "env" {
  type    = map(string)
  default = {}
}

variable "cpu_alloc" {
  type    = string
  default = "1000m"
}

variable "mem_alloc" {
  type    = string
  default = "128Mi"
}

variable "container_concurrency" {
  type    = number
  default = 80
}

variable "timeout_seconds" {
  type    = number
  default = 10
}

variable "ports" {
  type    = list(number)
  default = [80]
}

