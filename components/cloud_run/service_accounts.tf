resource "google_service_account" "cloud_run_sc" {
  account_id   = random_pet.cloud_run_sc.id
  display_name = random_pet.cloud_run_sc.id
  project      = data.terraform_remote_state.project.outputs.project_id
}
