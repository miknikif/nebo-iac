resource "google_project_iam_binding" "project" {
  project = data.terraform_remote_state.project.outputs.project_id
  role    = "roles/secretmanager.secretAccessor"
  members = ["serviceAccount:${google_service_account.cloud_run_sc.email}"]
}

resource "google_cloud_run_service_iam_member" "member" {
  location = google_cloud_run_service.webapp.location
  project  = google_cloud_run_service.webapp.project
  service  = google_cloud_run_service.webapp.name
  role     = "roles/run.invoker"
  member   = "allUsers"
}
