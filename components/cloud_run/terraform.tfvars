service_name   = "webapp"
service_region = "us-east1"
autoscaling = {
  min = 0
  max = 10
}
image                 = "gcr.io/nebo-03c2/webapp:v0.0.1"
ports                 = [80]
cpu_alloc             = "1000m"
mem_alloc             = "128Mi"
container_concurrency = 80
timeout_seconds       = 10
