resource "google_secret_manager_secret" "secrets" {
  for_each = var.env

  secret_id = each.key
  labels    = local.labels
  project   = data.terraform_remote_state.project.outputs.project_id
  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret_version" "secrets" {
  for_each = var.env

  secret      = google_secret_manager_secret.secrets[each.key].id
  secret_data = each.value
}
