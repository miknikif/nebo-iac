<!-- BEGIN_TF_DOCS -->


### Requirements

| Name | Version |
|------|---------|
| terraform | = 1.2.5 |
| google | >= 4.29.0 |
| random | 3.3.2 |

### Providers

| Name | Version |
|------|---------|
| google | >= 4.29.0 |
| random | 3.3.2 |
| terraform | n/a |

### Resources

| Name | Type |
|------|------|
| [google_cloud_run_service.webapp](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_service) | resource |
| [google_cloud_run_service_iam_member.member](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_service_iam_member) | resource |
| [google_project_iam_binding.project](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_binding) | resource |
| [google_secret_manager_secret.secrets](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/secret_manager_secret) | resource |
| [google_secret_manager_secret_version.secrets](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/secret_manager_secret_version) | resource |
| [google_service_account.cloud_run_sc](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [random_pet.cloud_run_sc](https://registry.terraform.io/providers/hashicorp/random/3.3.2/docs/resources/pet) | resource |
| [terraform_remote_state.project](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| autoscaling | n/a | <pre>object({<br>    min = number<br>    max = number<br>  })</pre> | <pre>{<br>  "max": 100,<br>  "min": 0<br>}</pre> | no |
| container\_concurrency | n/a | `number` | `80` | no |
| cpu\_alloc | n/a | `string` | `"1000m"` | no |
| env | n/a | `map(string)` | `{}` | no |
| image | n/a | `string` | `"gcr.io/nebo-03c2/webapp:v0.0.1"` | no |
| mem\_alloc | n/a | `string` | `"128Mi"` | no |
| ports | n/a | `list(number)` | <pre>[<br>  80<br>]</pre> | no |
| service\_name | n/a | `string` | `"app"` | no |
| service\_region | n/a | `string` | `"us-east1"` | no |
| timeout\_seconds | n/a | `number` | `10` | no |

### Modules

No modules.

### Outputs

No outputs.



---

> Written by the [SoftServe](https://softserveinc.com) team
<!-- END_TF_DOCS -->