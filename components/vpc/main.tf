/**
 * # Google Cloud VPC
 *
 * ## Change history
 *
 * | Version | Date       | Name              | Description     |
 * |:-------:|------------|-------------------|-----------------|
 * |  v 1.0  | 07/22/2022 | Mykhailo Kravtsov | Initial version |
 * |  v 1.1  | xx/xx/xxxx | -                 | -               |
 *
 * ## Module Structure
 *
 * - VPC can automatically set up your virtual topology, configuring prefix ranges for your subnets and network policies, or you can configure your own. You can also expand CIDR ranges without downtime
 * - A proxy-only subnet provides a pool of proxies that are reserved exclusively for Envoy proxies used by load balancers. It cannot be used for any other purposes. At any point, only one proxy-only subnet can be active in each region of a VPC network. The proxies terminate incoming connections and then evaluate where each HTTP(S) request should go based on the URL map, the backend service's session affinity, the balancing mode of each backend instance group or NEG, and other factors.
 *
 * ### Description
 * - This execution point automates the provision of all required subnets and proxy-only subnets in production environment
 * - The variables project_id is fetching from data block that is in terraform backend and data files.
 * - The other variables are defined in common_variables.tf and variables.tf files. Value has to be passed from terraform.tfvars
 *
 * ### File structure
 * - Below are the following terraform files at this execution folder.
 *   - `backend.tf` - Has backend configuration to store the state files in the GCS bucket. Attribute "prefix" gives the path where the state files are stored. Have several data resources to load information about kernel/rtm/rws (if present) instance groups, to add them into backends.
 *   - `main.tf` - Has module description and vpc module
 *   - `outputs.tf` - Has output variables that are displayed upon successful creation of the resources and can be used in other modules.
 *   - `provider.tf` - In this file the provider information are declared.
 *   - `terraform.tfvars` - Holds the value of variables that are needed for successful creation of the resources.
 *   - `variables.tf` - Holds the value of variables that are needed for successful creation of the resources.
 *   - `version.tf` - Ensuring that we will be able to use only v1.2.5
 */

#Module to create Network and Enabling SVPC Host Project
module "gke_vpc" {
  source          = "terraform-google-modules/network/google"
  version         = "= 5.1.0"
  project_id      = data.terraform_remote_state.project.outputs.project_id
  network_name    = random_pet.vpc.id
  shared_vpc_host = false
  subnets = [{
    subnet_name   = "${random_pet.vpc.id}-${var.region}"
    subnet_ip     = "10.10.0.0/16"
    subnet_region = var.region
  }]
  secondary_ranges = {
    "${random_pet.vpc.id}-${var.region}" = [{
      range_name    = "${random_pet.vpc.id}-${var.region}-pods"
      ip_cidr_range = "10.11.0.0/16"
      }, {
      range_name    = "${random_pet.vpc.id}-${var.region}-services"
      ip_cidr_range = "10.12.0.0/16"
    }]
  }
  firewall_rules = [{
    name        = "allow-all-10"
    description = "Allow Pod to Pod connectivity"
    direction   = "INGRESS"
    ranges      = ["10.0.0.0/8"]
    allow = [{
      protocol = "tcp"
      ports    = ["0-65535"]
    }]
  }]

}
