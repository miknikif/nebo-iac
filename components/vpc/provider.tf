terraform {
  required_providers {
    google = {
      version = ">= 3.50.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.3.2"
    }
  }
}
