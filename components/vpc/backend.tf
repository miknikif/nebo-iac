terraform {
  backend "gcs" {
    bucket = "tf-state-nebo-us-east-1"
    prefix = "components/vpc"
  }
}

data "terraform_remote_state" "project" {
  backend = "gcs"
  config = {
    bucket = "tf-state-nebo-us-east-1"
    prefix = "project"
  }
}
