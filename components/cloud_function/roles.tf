resource "google_project_iam_binding" "project" {
  project = data.terraform_remote_state.project.outputs.project_id
  role    = "roles/cloudfunctions.invoker"
  members = ["serviceAccount:${google_service_account.cron_app_sc.email}"]
}
