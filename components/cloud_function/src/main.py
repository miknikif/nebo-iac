def make_request(request):
    import requests
    from os import environ
    from requests.auth import HTTPBasicAuth

    # Get vars
    url = environ.get('URL', '')
    user = environ.get('BASIC_AUTH_USER', '')
    password = environ.get('BASIC_AUTH_PASSWORD', '')

    response = requests.get(url, auth=HTTPBasicAuth(user, password))
    response.raise_for_status()
    return str(response.content)
