data "archive_file" "app_archive" {
  type        = "zip"
  source_dir  = "${path.module}/src/"
  output_path = "${path.module}/${local.app_name}-src.zip"
}
