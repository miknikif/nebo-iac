locals {
  app_name = "cron-app"

  labels = {
    owner   = "mykhailo-nikiforov"
    app     = local.app_name
    project = "nebo"
  }
}
