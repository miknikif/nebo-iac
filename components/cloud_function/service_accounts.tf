resource "google_service_account" "cron_app_sc" {
  account_id   = local.app_name
  display_name = local.app_name
  project      = data.terraform_remote_state.project.outputs.project_id
}
