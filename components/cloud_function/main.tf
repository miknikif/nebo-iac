resource "google_cloudfunctions_function" "cron_app" {
  name        = local.app_name
  description = "nebo function"
  runtime     = "python310"
  region      = var.region
  project     = data.terraform_remote_state.project.outputs.project_id

  available_memory_mb   = 128
  trigger_http          = true
  entry_point           = "make_request"
  source_archive_bucket = google_storage_bucket.app_src.name
  source_archive_object = google_storage_bucket_object.app_src_archive.name
  max_instances         = 1
  timeout               = 15
  environment_variables = {
    "BASIC_AUTH_PASSWORD" = data.terraform_remote_state.gke.outputs.basic_auth_pass
    "BASIC_AUTH_USER"     = data.terraform_remote_state.gke.outputs.basic_auth_login
    "URL"                 = data.terraform_remote_state.gke.outputs.app_url
  }

  labels = local.labels
}
