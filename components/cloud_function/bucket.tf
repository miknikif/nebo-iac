resource "google_storage_bucket" "app_src" {
  name     = "${local.app_name}-src"
  project  = data.terraform_remote_state.project.outputs.project_id
  location = "US"
}

resource "google_storage_bucket_object" "app_src_archive" {
  depends_on = [data.archive_file.app_archive]

  name   = "${local.app_name}-src.zip"
  bucket = google_storage_bucket.app_src.name
  source = "${path.module}/${local.app_name}-src.zip"
}
