resource "google_cloud_scheduler_job" "cron_app" {
  name        = local.app_name
  description = local.app_name
  schedule    = "*/5 * * * *"
  time_zone   = "Europe/Kiev"

  http_target {
    http_method = "GET"
    uri         = google_cloudfunctions_function.cron_app.https_trigger_url

    oidc_token {
      audience              = google_cloudfunctions_function.cron_app.https_trigger_url
      service_account_email = google_service_account.cron_app_sc.email
    }
  }

  retry_config {
    max_backoff_duration = "3600s"
    max_doublings        = 5
    max_retry_duration   = "0s"
    min_backoff_duration = "5s"
    retry_count          = 0
  }
}
