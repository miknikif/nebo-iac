<!-- BEGIN_TF_DOCS -->


### Requirements

| Name | Version |
|------|---------|
| terraform | = 1.2.5 |
| archive | 2.2.0 |
| google | >= 4.29.0 |
| random | 3.3.2 |

### Providers

| Name | Version |
|------|---------|
| archive | 2.2.0 |
| google | >= 4.29.0 |
| terraform | n/a |

### Resources

| Name | Type |
|------|------|
| [google_cloud_scheduler_job.cron_app](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_scheduler_job) | resource |
| [google_cloudfunctions_function.cron_app](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloudfunctions_function) | resource |
| [google_project_iam_binding.project](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_binding) | resource |
| [google_service_account.cron_app_sc](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_storage_bucket.app_src](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket_object.app_src_archive](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_object) | resource |
| [archive_file.app_archive](https://registry.terraform.io/providers/hashicorp/archive/2.2.0/docs/data-sources/file) | data source |
| [terraform_remote_state.gke](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |
| [terraform_remote_state.project](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| region | function region | `string` | `"us-east1"` | no |

### Modules

No modules.

### Outputs

No outputs.



---

> Written by the [SoftServe](https://softserveinc.com) team
<!-- END_TF_DOCS -->