terraform {
  required_providers {
    google = {
      version = ">= 4.29.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.3.2"
    }
    archive = {
      source  = "hashicorp/archive"
      version = "2.2.0"
    }
  }
}
